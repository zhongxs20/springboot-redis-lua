package com.middleware;

import com.middleware.redis.RedisService;
import com.middleware.server.RedisLuaServer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static com.middleware.redis.RedisKeys.ACTIVITY_NUMBER;

/**
 * @author Mr.Zhong
 * @description redis lua 测试类
 * @date 2022/04/20 17:11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class RedisLuaApplicationTests {

    @Autowired
    private RedisLuaServer redisLuaServer;

    @Autowired
    private RedisService redisService;

    /**
     * @description redis lua 限流测试
     * @author Mr.Zhong
     * @date 2022/04/20 16:48
     */
    @Test
    void mobileArmor() {
        //  手机号
        String mobile = "18888888888";
        //  时间间隔
        int time = 1;
        for (int i = 0; i < 100; i++) {
            Boolean bl = redisLuaServer.mobileArmor(mobile, time);
            if (bl) {
                System.out.println("请求成功！");
            } else {
                System.out.println("操作太频繁请稍等！");
            }
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * @description redis lua 秒杀无锁扣减库存测试
     * @author Mr.Zhong
     * @date 2022/04/20 16:49
     */
    @Test
    void seckill() {
        //  活动编号
        String activityId = "31f24f6e72ab48eab1d728dea68f6b95";
        //  扣减库存数量
        int amount = 1;
        //  添加库存为10过期时间为180秒
        redisService.setCacheObject(ACTIVITY_NUMBER + activityId, 10, 180L, TimeUnit.SECONDS);
        //  启动一百个线程去抢库存
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                run(activityId, amount);
            }, "th-pool").start();
        }
        //  主线程等待两秒
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void run(String activityId, int amount) {
        Long rel = redisLuaServer.seckill(activityId, amount);
        switch (rel + "") {
            case "0":
                System.out.println("库存不足(活动下架)");
                break;
            case "1":
                System.out.println("扣减库存成功");
                break;
            case "-1":
                System.out.println("扣减库存成功,库存不足(活动下架)");
                break;
        }
    }


}
