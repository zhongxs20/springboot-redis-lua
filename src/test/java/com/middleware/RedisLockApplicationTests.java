package com.middleware;


import com.middleware.server.RedisLockServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Mr.Zhong
 * @description 分布式锁测试类
 * @date 2022/04/20 17:10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisLockApplicationTests {

    @Autowired
    private RedisLockServer redisLockServer;


    /**
     * redis分布式锁测试方法
     */
    @Test
    public void distributedTest() {
        String userId = "1d728dea68f6b95";
        //  启动一百个线程占用分布式锁
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                run(userId);
            }, "th-pool").start();
        }
        //  主线程等待三秒
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void run(String userId) {
        redisLockServer.distributed(userId);
        System.out.println("当前锁空闲！");
    }


}
