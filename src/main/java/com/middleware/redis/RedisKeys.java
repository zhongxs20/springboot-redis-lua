package com.middleware.redis;

/**
 * @author Mr.Zhong
 * @description key
 * @date 2022/04/20 17:01
 */
public interface RedisKeys {


    /**
     * 活动库存
     */
    String ACTIVITY_NUMBER = "ACTIVITY_NUMBER";


}
