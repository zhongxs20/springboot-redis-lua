package com.middleware.redis;


import org.springframework.data.redis.core.script.DigestUtils;
import org.springframework.data.redis.core.script.RedisScript;

/**
 * @author Mr.Zhong
 * @description
 * @date 2022/04/20 14:35
 */
public class RedisScriptImpl<T> implements RedisScript<T> {

    private final String script;
    private final String sha1;
    private final Class<T> resultType;


    public RedisScriptImpl(String script, Class<T> resultType) {
        this.script = script;
        this.sha1 = DigestUtils.sha1DigestAsHex(script);
        this.resultType = resultType;
    }

    @Override
    public String getSha1() {
        return sha1;
    }

    @Override
    public Class<T> getResultType() {
        return resultType;
    }

    @Override
    public String getScriptAsString() {
        return script;
    }

}
