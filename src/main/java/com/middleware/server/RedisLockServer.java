package com.middleware.server;


/**
 * @author Mr.Zhong
 * @description redis 分布式锁实现
 * @date 2022/04/20 16:56
 */
public interface RedisLockServer {

    /**
     * 分布式锁测试方法实现
     *
     * @param userId
     */
    void distributed(String userId);


}
