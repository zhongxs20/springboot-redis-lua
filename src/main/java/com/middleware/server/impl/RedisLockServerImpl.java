package com.middleware.server.impl;


import com.middleware.server.RedisLockServer;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.stereotype.Service;

/**
 * @author Mr.Zhong
 * @description redis分布式锁实现类
 * @date 2022/04/20 16:57
 */
@Service
public class RedisLockServerImpl implements RedisLockServer {


    /**
     * Klock可以标注四个参数，作用分别如下
     * <p>
     * name：lock的name，对应redis的key值。默认为：类名+方法名
     * <p>
     * lockType：锁的类型，目前支持（可重入锁，公平锁，读写锁）。默认为：公平锁
     * <p>
     * waitTime：获取锁最长等待时间。默认为：60s。同时也可通过spring.klock.waitTime统一配置
     * <p>
     * leaseTime：获得锁后，自动释放锁的时间。默认为：60s。同时也可通过spring.klock.leaseTime统一配置
     * <p>
     * lockTimeoutStrategy: 加锁超时的处理策略，可配置为不做处理、快速失败、阻塞等待的处理策略，默认策略为不做处理
     * <p>
     * customLockTimeoutStrategy: 自定义加锁超时的处理策略，需指定自定义处理的方法的方法名，并保持入参一致。
     * <p>
     * releaseTimeoutStrategy: 释放锁时，持有的锁已超时的处理策略，可配置为不做处理、快速失败的处理策略，默认策略为不做处理
     * <p>
     * customReleaseTimeoutStrategy: 自定义释放锁时，需指定自定义处理的方法的方法名，并保持入参一致。
     *
     * @param userId userId
     */
    @Klock(keys = {"#userId"})
    @Override
    public void distributed(String userId) {
        System.out.println("当前锁被占用！");
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
