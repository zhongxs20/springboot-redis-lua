package com.middleware.server;


/**
 * redis lua业务
 *
 * @author Mr.Zhong
 * @description
 * @date 2022/04/20 14:49
 */
public interface RedisLuaServer {

    /**
     * 根据手机号限制请求次数
     * redis、lua实现访问限制
     *
     * @param mobileNo 手机号
     * @param time     间隔时间/秒,1为一秒内只能请求一次
     * @return Boolean
     */
    Boolean mobileArmor(String mobileNo, int time);

    /**
     * redis lua实现秒杀扣减库存业务
     *
     * @param activityId 活动编号或ID
     * @param amount     扣减库存数
     * @return
     */
    Long seckill(String activityId, int amount);

}
